@base <https://purl.org/coscine/ap/sfb1394/Immersion/> .

@prefix dash: <http://datashapes.org/dash#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix qudt: <http://qudt.org/schema/qudt/> .
@prefix unit: <http://qudt.org/vocab/unit/> .
@prefix csmd: <http://www.purl.org/net/CSMD/4.0#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix sfb1394: <http://purl.org/coscine/terms/sfb1394#> .
@prefix coscineSfb1394Immersion: <https://purl.org/coscine/ap/sfb1394/Immersion#> .

 # Immersion 
<https://purl.org/coscine/ap/sfb1394/Immersion/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de/> ;
  dcterms:rights "Copyright © 2020 IT Center, RWTH Aachen University" ;
  dcterms:title "SFB1394 Immersion"@en ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/sfb1394/Immersion/> ;
  sh:closed true ;

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineSfb1394Immersion:ID ;
  sh:property coscineSfb1394Immersion:aliasID ;
  sh:property coscineSfb1394Immersion:user ;
  sh:property coscineSfb1394Immersion:date ;
  sh:property coscineSfb1394Immersion:affiliation ;
  sh:property coscineSfb1394Immersion:dois ;
  sh:property coscineSfb1394Immersion:temperature ;
  sh:property coscineSfb1394Immersion:relativeHumidity ;
  sh:property coscineSfb1394Immersion:environmentalGas ;
  sh:property coscineSfb1394Immersion:operator ;
  sh:property coscineSfb1394Immersion:instrumentId ;
  sh:property coscineSfb1394Immersion:immersionExperimentRoutine ;
  sh:property coscineSfb1394Immersion:electrolytePh ;
  sh:property coscineSfb1394Immersion:electrolyteCondition ;
  sh:property coscineSfb1394Immersion:flowRate ;
  sh:property coscineSfb1394Immersion:revolutionsPerMinute ;
  sh:property coscineSfb1394Immersion:volume ;
  sh:property coscineSfb1394Immersion:comments ;
.

coscineSfb1394Immersion:ID 
  sh:path dcterms:identifier ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:name "ID"@en, "ID"@de ;
. 

coscineSfb1394Immersion:aliasID 
  sh:path sfb1394:aliasID ;
  sh:order 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "External/alias ID"@en, "External/alias ID"@de ;
. 

coscineSfb1394Immersion:user 
  sh:path dcterms:creator ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:minLength 1 ;
  sh:datatype xsd:string ;
  sh:defaultValue "{ME}" ;
  sh:name "User"@en, "User"@de ;
. 

coscineSfb1394Immersion:date 
  sh:path dcterms:created ;
  sh:order 3 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:date ;
  sh:defaultValue "{TODAY}" ;
  sh:name "Date"@en, "Date"@de ;
. 

coscineSfb1394Immersion:affiliation 
  sh:path sfb1394:affiliation ;
  sh:order 4 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Affiliation"@en, "Affiliation"@de ;
. 

coscineSfb1394Immersion:dois 
  sh:path dcterms:isReferencedBy ;
  sh:order 5 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  dash:singleLine false ;
  sh:name "DOIs"@en, "DOIs"@de ;
. 

coscineSfb1394Immersion:temperature 
  sh:path sfb1394:temperature ;
  sh:order 6 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Temperature [°C]"@en, "Temperature [°C]"@de ;
  qudt:Unit unit:Deg_C ;
. 

coscineSfb1394Immersion:relativeHumidity 
  sh:path sfb1394:relativeHumidity ;
  sh:order 7 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Relative humidity [%]"@en, "Relative humidity [%]"@de ;
  qudt:Unit unit:PERCENT_RH ;
. 

coscineSfb1394Immersion:environmentalGas 
  sh:path sfb1394:environmentalGas ;
  sh:order 8 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Environmental gas"@en, "Environmental gas"@de ;
. 

coscineSfb1394Immersion:operator 
  sh:path sfb1394:operator ;
  sh:order 9 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Operator"@en, "Operator"@de ;
. 

coscineSfb1394Immersion:instrumentId 
  sh:path csmd:investigation_instrument ;
  sh:order 10 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Instrument ID"@en, "Instrument ID"@de ;
. 

coscineSfb1394Immersion:immersionExperimentRoutine 
  sh:path sfb1394:immersionExperimentRoutine ;
  sh:order 11 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Immersion experiment routine"@en, "Immersion experiment routine"@de ;
. 

coscineSfb1394Immersion:electrolytePh 
  sh:path sfb1394:electrolytePh ;
  sh:order 12 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Electrolyte pH"@en, "Electrolyte pH"@de ;
  qudt:Unit unit:PH ;
. 

coscineSfb1394Immersion:electrolyteCondition 
  sh:path sfb1394:electrolyteCondition ;
  sh:order 13 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Electrolyte condition"@en, "Electrolyte condition"@de ;
. 

coscineSfb1394Immersion:flowRate 
  sh:path sfb1394:flowRate ;
  sh:order 14 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Flow rate [ml/h]"@en, "Flow rate [ml/h]"@de ;
  qudt:Unit unit:MilliL-PER-HR ;
. 

coscineSfb1394Immersion:revolutionsPerMinute 
  sh:path sfb1394:revolutionsPerMinute ;
  sh:order 15 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Revolutions per minute [rpm]"@en, "Revolutions per minute [rpm]"@de ;
  qudt:Unit unit:PER-MIN ;
. 

coscineSfb1394Immersion:volume 
  sh:path sfb1394:volume ;
  sh:order 16 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Volume [ml]"@en, "Volume [ml]"@de ;
  qudt:Unit unit:MilliL ;
. 

coscineSfb1394Immersion:comments 
  sh:path sfb1394:comments ;
  sh:order 17 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  dash:singleLine false ;
  sh:name "Comments"@en, "Comments"@de ;
. 
