@base <https://purl.org/coscine/ap/vvm/ind/> .

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix dash: <http://datashapes.org/dash#> .

@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix ind: <https://purl.org/coscine/ap/vvm/ind/> .

@prefix coscineind: <https://purl.org/coscine/ap/vvm/ind/#> .

<https://purl.org/coscine/ap/vvm/ind/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://itc.rwth-aachen.de/> ;
  dcterms:rights "Copyright © 2022 IT Center, RWTH Aachen University" ;
  dcterms:title "inD VVM application profile"@en ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/vvm/ind/> ;
  sh:closed true ; # no additional properties are allowed

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineind:cn ;
  sh:property coscineind:cv ;
  sh:property coscineind:dts ;
  sh:property coscineind:dte ;
  sh:property coscineind:fv ;
  sh:property coscineind:fn ;
  sh:property coscineind:nb ;
  sh:property coscineind:ne ;
  sh:property coscineind:rrn ;
  sh:property coscineind:rcn ;
  sh:property coscineind:prn ;
  sh:property coscineind:rplo ;
  sh:property coscineind:rpla ;
  sh:property coscineind:st ;
  sh:property coscineind:cp ;
  sh:property coscineind:in ;
  sh:property coscineind:rm ;
  sh:property coscineind:co .

  coscineind:cn
    sh:path ind:converterName ;
    sh:order 0 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Converter Name"@en, "Kontvertername"@de .
  
  coscineind:cv
    sh:path ind:converterVersion ;
    sh:order 1 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Converter Version"@en, "Kontverterversion"@de .

  coscineind:dts
    sh:path ind:dayTimeStart ;
    sh:order 2 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Start Day Time"@en, "Anfang - Tag und Uhrzeit"@de .

  coscineind:dte
    sh:path ind:dayTimeEnd ;
    sh:order 3 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "End Day Time"@en, "Ende - Tag und Uhrzeit"@de .

  coscineind:fv
    sh:path ind:formatVersion ;
    sh:order 4 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Format Version"@en, "Formatversion"@de .

  # replace with vocabulary
  coscineind:fn
    sh:path ind:formatName ;
    sh:order 5 ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Format Name"@en, "Formatname"@de .

  coscineind:nb
    sh:path ind:naturalBehavior ;
    sh:order 6 ;
    sh:maxCount 1 ;
    sh:datatype xsd:boolean ;
    sh:name "Natural Behavior"@en, "Natural Behavior"@de .

  coscineind:ne
    sh:path ind:naturalExposure ;
    sh:order 7 ;
    sh:maxCount 1 ;
    sh:datatype xsd:boolean ;
    sh:name "Natural Exposure"@en, "Natural Exposure"@de .

  coscineind:rrn
    sh:path ind:recorderNumber ;
    sh:order 8 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Recorder Number"@en, "Recorder Number"@de .

  coscineind:rcn
    sh:path ind:recordingNumber ;
    sh:order 9 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Recording Number"@en, "Recording Number"@de .

  coscineind:prn
    sh:path ind:parentRecordingNumber ;
    sh:order 10 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Parent Recording Number"@en, "Parent Recording Number"@de .

  coscineind:rplo
    sh:path ind:referencePointLon ;
    sh:order 11 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Reference Point Latitude"@en, "Reference Point Latitude"@de .

  coscineind:rpla
    sh:path ind:referencePointLat ;
    sh:order 12 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    sh:name "Reference Point Longitude"@en, "Reference Point Longitude"@de .

  coscineind:st
    sh:path ind:scenarioType ;
    sh:order 13 ;
    sh:datatype xsd:string ;
    sh:name "Scenario Type"@en, "Grundszenario"@de .

  coscineind:cp
    sh:path ind:criticalityPhenomena ;
    sh:order 14 ;
    sh:name "Criticality Phenomena"@en, "Kritikalitätsphänomen"@de ;
    sh:class <http://purl.org/auto/criticality_phenomena#Criticality_Phenomenon> .

  coscineind:in
    sh:path ind:intersection ;
    sh:order 15 ;
    sh:datatype xsd:string ;
    sh:name "Intersection Location"@en, "Ort der Kreuzung"@de .

  # replace with vocabulary
  coscineind:rm
    sh:path ind:refModality ;
    sh:order 16 ;
    sh:datatype xsd:string ;
    sh:name "Reference Modality"@en, "Referenzmodalität"@de .

  coscineind:co
    sh:path ind:comment ;
    sh:order 17 ;
    sh:maxCount 1 ;
    sh:datatype xsd:string ;
    dash:singleLine false ;
    sh:name "Comment"@en, "Komentar"@de .
